# creates: 1scogef.png

# Copyright (C) 2016-2019
# See accompanying license files for details.

from ase.atoms import Atoms
from ase.optimize import FIRE
from ase.calculators.emt import EMT

fmax = 0.01

image = Atoms('AuAgAg', positions=((-1, 0, 0), (0, 0, 0), (1, 0, 0)))
image.set_calculator(EMT())
FIRE(image).run(fmax=fmax)

from cogef import COGEF


cogef = COGEF(0, 2, optimizer=FIRE, fmax=fmax)
cogef.images = [image]
stepsize = 0.1
steps = 30
cogef.pull(stepsize, steps)

from ase.units import J, m

f = m / J * 1e9  # eV / A -> nN

print('Maximal force: {0:4.2f} nN (from energies),'.format(
    cogef.get_maximum_force(method='use_energies') * f) +
      ' {0:4.2f} nN (from forces)'.format(
          cogef.get_maximum_force(method='use_forces') * f))

# Temperature -----------------------------------------------------

from cogef import Dissociation
import pylab as plt

LOADING_RATE = 10  # [nN/s]
T = 300      # Temperature [K]
P = 101325.  # Pressure    [Pa]

plt.figure()
diss = Dissociation(cogef,
                    force_unit='nN')  # To have loading rate in nN/s and
                                      # all forces in nN in the input and
                                      # output of Dissociation methods
fstep = 0.003
for T in [5, 50, 100, 200, 300, 500]:
    if 0:
        # Manual limits
        fmin, fmax = 0.2, 2
        DPDF, F = diss.probability_density(
            T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
        p = plt.plot(F, DPDF, '-', label=None)
        color = p[0].get_color()
    else:
        color = None
    # Automatic limits
    fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE, force_step=fstep,
                                       method='electronic')
    DPDF, F = diss.probability_density(
        T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
    p = plt.plot(F, DPDF, 'o-', color=color,
                 label='T={0}K'.format(T))
    plt.axvline(x=cogef.get_maximum_force(method='use_energies') * f,
                ls='--', color='k')

plt.legend(loc=2)
plt.ylim(0, 110)
plt.xlabel('F [nN]')
plt.ylabel('dp/dF [nN$^{-1}$]')
plt.savefig('1scogef.png')

# ------------------------------------

if 1:
    # Only picture
    import sys
    sys.exit()

for T in [5, 50, 100, 200, 300, 500]:
    fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE, force_step=fstep,
                                       method='electronic')
    force, error = diss.rupture_force_and_uncertainty(
        T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
    print('Rupture force ({0}K): ({1:4.2f} +- {2:4.2f}) nN'.format(
        T, force, error))

# ------------------------------------

# Remove the files
from os import remove
steps = 30
for i in range(1, steps + 1):
    try:
        remove('cogef' + str(i) + '.traj')
    except OSError:
        pass

T = 300
fmax = 0.01
stepsize = 0.1
max_steps = 100

image = Atoms('AuAgAg', positions=((-1, 0, 0), (0, 0, 0), (1, 0, 0)))
image.set_calculator(EMT())
FIRE(image).run(fmax=fmax)

cogef = COGEF([image], 0, 2, optimizer=FIRE, fmax=fmax)
diss = Dissociation(cogef, force_unit='nN')
for step in range(max_steps + 1):
    try:
        fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE,
                                           force_step=fstep,
                                           method='electronic')
        break
    except ValueError as msg:
        if diss.error not in [1, 2, 3]:
            raise ValueError(msg)
        assert step < max_steps, 'Step limit reached.'
        # Add one image
        cogef.pull(stepsize, 1, initialize, 'cogef.traj')
force, error = diss.rupture_force_and_uncertainty(
    T, P, LOADING_RATE, fmax, fmin, fstep, method='electronic')
print('Rupture force: ({0:4.2f} +- {1:4.2f}) nN'.format(force, error))


def initialize_vib(image, dirname):
    """Initialize the image for vibrational analysis.

    """
    image.set_calculator(EMT())


diss = Dissociation(cogef, initialize_vib, vib_method='frederiksen',
                    force_unit='nN')
diss.geometry = 'linear'  # Like in class IdealGasThermo

fmin, fmax = diss.get_force_limits(T, P, LOADING_RATE, force_step=fstep,
                                   method='Gibbs')
force, error = diss.rupture_force_and_uncertainty(
    T, P, LOADING_RATE, fmax, fmin, fstep, method='Gibbs')
print('Rupture force: ({0:4.3f} +- {1:4.3f}) nN'.format(force, error))

# ------------------------------------


# Remove the files
steps = 30
for i in range(1, steps + 1):
    try:
        remove('cogef' + str(i) + '.traj')
    except OSError:
        pass
