from ase.units import J, m
from cogef import COGEF

f = m / J * 1e9  # eV / A -> nN

cogef = COGEF(0, 2)  # this reads cogef_0_2/cogef.traj if it is there

print('Maximal force: {0:4.2f} nN (from energies),'.format(
    cogef.get_maximum_force(method='use_energies') * f) +
    ' {0:4.2f} nN (from forces)'.format(
        cogef.get_maximum_force(method='use_forces') * f))
