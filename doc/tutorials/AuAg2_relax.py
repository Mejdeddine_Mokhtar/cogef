from ase.atoms import Atoms
from ase.optimize import FIRE
from ase.calculators.emt import EMT

fmax = 0.01

image = Atoms('AuAgAg', positions=((-1, 0, 0), (0, 0, 0), (1, 0, 0)))
image.set_calculator(EMT())
FIRE(image).run(fmax=fmax)
image.write('AuAg2.traj')
